/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.javachallenge.gossipdriver;

import com.javachallenge.gossipdriver.model.Driver;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ManageGossipAmongDriversTest {
  private Driver driver1;
  Driver driver2;
  Driver driver3;
  Driver driver4;
  Driver driver5;
  Driver driver6;
  Driver driver7;
  Driver driver8;
  Driver driver9;
  Driver driver10;
  Driver driver11;
  Driver driver12;
  Driver driver13;
  Driver driver14;
  Driver driver15;
  Driver driver16;
  Driver driver17;
  Driver driver18;
  Driver driver19;
  Driver driver20;
  Driver driver21;
  Driver driver22;
  Driver driver23;
  Driver driver24;
  Driver driver25;
  Driver driver26;
  Driver driver27;
  Driver driver28;
  Driver driver29;
  Driver driver30;

  List<Driver> driversList = new ArrayList<Driver>();
  ManageGossipAmongDrivers gossipManage;

  @Before
  public void beforeDriverTest() {
    final int[] route1 = {
        3, 1, 2, 3
    };
    final int[] route2 = {
        3, 2, 3, 1
    };
    final int[] route3 = {
        4, 2, 3, 4, 5
    };
    final int[] route4 = {
        5, 9, 8, 2, 5, 4
    };
    final int[] route5 = {
        5, 2, 3
    };
    final int[] route6 = {
        7, 8, 3
    };
    final int[] route7 = {
        5, 8, 3
    };

    final int[] route8 = {
        2, 1, 2
    };
    final int[] route9 = {
        5, 2, 8
    };

    final int[] route10 = {
        7, 11, 2, 2, 4, 8, 2, 2
    };
    final int[] route11 = {
        3, 0, 11, 8
    };
    final int[] route12 = {
        5, 11, 8, 10, 3, 11
    };
    final int[] route13 = {
        5, 9, 2, 5, 0, 3

    };
    final int[] route14 = {
        7, 4, 8, 2, 8, 1, 0, 5
    };
    final int[] route15 = {
        3, 6, 8, 9
    };
    final int[] route16 = {
        4, 2, 11, 3, 3
    };

    final int[] route17 = {
        12, 23, 15, 2, 8, 20, 21, 3, 23, 3, 27, 20, 0
    };
    final int[] route18 = {
        21, 14, 8, 20, 10, 0, 23, 3, 24, 23, 0, 19, 14, 12, 10, 9, 12, 12, 11, 6, 27, 5
    };
    final int[] route19 = {
        8, 18, 27, 10, 11, 22, 29, 23, 14
    };
    final int[] route20 = {
        13, 7, 14, 1, 9, 14, 16, 12, 0, 10, 13, 19, 16, 17
    };
    final int[] route21 = {
        24, 25, 21, 4, 6, 19, 1, 3, 26, 11, 22, 28, 14, 14, 27, 7, 20, 8, 7, 4, 1, 8, 10, 18, 21
    };
    final int[] route22 = {
        13, 20, 26, 22, 6, 5, 6, 23, 26, 2, 21, 16, 26, 24
    };
    final int[] route23 = {
        6, 7, 17, 2, 22, 23, 21
    };

    final int[] route24 = {
        23, 14, 22, 28, 10, 23, 7, 21, 3, 20, 24, 23, 8, 8, 21, 13, 15, 6, 9, 17, 27, 17, 13, 14
    };
    final int[] route25 = {
        23, 13, 1, 15, 5, 16, 7, 26, 22, 29, 17, 3, 14, 16, 16, 18, 6, 10, 3, 14, 10, 17, 27, 25
    };

    final int[] route26 = {
        25, 28, 5, 21, 8, 10, 27, 21, 23, 28, 7, 20, 6, 6, 9, 29, 27, 26, 24, 3, 12, 10, 21, 10, 12,
        17
    };
    final int[] route27 = {
        26, 22, 26, 13, 10, 19, 3, 15, 2, 3, 25, 29, 25, 19, 19, 24, 1, 26, 22, 10, 17, 19, 28, 11,
        22, 2, 13
    };
    final int[] route28 = {
        8, 4, 25, 15, 20, 9, 11, 3, 19
    };
    final int[] route29 = {
        24, 29, 4, 17, 2, 0, 8, 19, 11, 28, 13, 4, 16, 5, 15, 25, 16, 5, 6, 1, 0, 19, 7, 4, 6

    };
    final int[] route30 = {
        16, 25, 15, 17, 20, 27, 1, 11, 1, 18, 14, 23, 27, 25, 26, 17, 1
    };

    driver1 = new Driver(22, route1);
    driver2 = new Driver(33, route2);
    driver3 = new Driver(44, route3);
    driver4 = new Driver(44, route4);
    driver5 = new Driver(55, route5);
    driver6 = new Driver(66, route6);
    driver7 = new Driver(77, route7);
    driver8 = new Driver(88, route8);
    driver9 = new Driver(99, route9);
    driver10 = new Driver(22, route10);
    driver11 = new Driver(33, route11);
    driver12 = new Driver(44, route12);
    driver13 = new Driver(44, route13);
    driver14 = new Driver(55, route14);
    driver15 = new Driver(66, route15);
    driver16 = new Driver(77, route16);

    driver17 = new Driver(22, route17);
    driver18 = new Driver(33, route18);
    driver19 = new Driver(44, route19);
    driver20 = new Driver(44, route20);
    driver21 = new Driver(55, route21);
    driver22 = new Driver(66, route22);
    driver23 = new Driver(77, route23);
    driver24 = new Driver(88, route24);
    driver25 = new Driver(99, route25);
    driver26 = new Driver(22, route26);
    driver27 = new Driver(33, route27);
    driver28 = new Driver(44, route28);
    driver29 = new Driver(44, route29);
    driver30 = new Driver(55, route30);

    gossipManage = new ManageGossipAmongDrivers();
    driversList.add(driver8);
    driversList.add(driver9);
    // driversList.add(driver3);

  }

  @Test // 5
  public void drivers_should_have_correct_number_of_gossips() {
    gossipManage.calculatingGossips2(Arrays.asList(driver1, driver2, driver3));
    assertTrue(driver1.doYouKnownThisNumberOfGossips(3));

  }

  @Test // 10
  public void drivers_should_have_correct_number_of_gossips10To16() {
    gossipManage.calculatingGossips2(
        Arrays.asList(driver10, driver11, driver12, driver13, driver14, driver15, driver16));
    assertTrue(driver10.doYouKnownThisNumberOfGossips(7));

  }

  //
  @Test // 1
  public void gossip_should_have_happend_between_the_drivers12_at_position1() {
    final boolean expect = gossipManage.isThereGossipAtThisStop(driver1, driver2, 1);
    assertTrue(expect);
  }

  @Test // 2
  public void gossip_should_have_happend_between_the_drivers23_at_position3() {
    final boolean expect = gossipManage.isThereGossipAtThisStop(driver2, driver3, 3);
    assertTrue(expect);
  }

  @Test // 3
  public void gossip_Should_Not_Happen_Between_The_Drivers13() {
    final Boolean expected = gossipManage.isThereGossipAtThisStop(driver1, driver3, 3);
    assertFalse(expected);

  }

  @Test // 9
  public void gossipShouldHappenBetweenTheDrivers10To16() {
    final int expected = gossipManage.calculatingGossips2(
        Arrays.asList(driver10, driver11, driver12, driver13, driver14, driver15, driver16));
    assertEquals(9, expected);

  }

  @Test // 4
  public void gossipShouldHappenBetweenTheDrivers123() {
    final int expected = gossipManage.calculatingGossips2(Arrays.asList(driver1, driver2, driver3));
    assertEquals(5, expected);

  }

  @Test // 10
  public void gossipShouldHappenBetweenTheDrivers17To30() {
    final int expected = gossipManage.calculatingGossips2(
        Arrays.asList(driver17, driver18, driver19, driver20, driver21, driver22, driver23,
            driver24, driver25, driver26, driver27, driver28, driver29, driver30));
    assertEquals(16, expected);

  }

  @Test // 8
  public void gossipShoulNotdHappenBetweenTheDrivers89() {
    final int expected = gossipManage.calculatingGossips2(Arrays.asList(driver8, driver9));
    assertEquals(-1, expected);
  }

  @Test // 7
  public void should_return_false_when_there_are_two_drivers_whit_one_gossip_each() {
    assertFalse(gossipManage.checkIfAllDriversKnowsAllGossips(Arrays.asList(driver1, driver2)));
  }

  @Test // 6
  public void should_return_true_when_there_is_only_one_driver() {
    assertTrue(gossipManage.checkIfAllDriversKnowsAllGossips(Arrays.asList(driver1)));
  }

}
