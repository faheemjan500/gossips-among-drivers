/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.javachallenge.gossipdriver.model;

import com.javachallenge.gossipdriver.model.Driver;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DriverTest {
  Driver driver1;
  Driver driver2;
  Driver driver3;
  Driver driver4;

  @Before
  public void beforeDriverTest() {
    final int[] route1 = {
        3, 1, 2, 3
    };
    final int[] route2 = {
        3, 2, 3, 1
    };
    final int[] route3 = {
        4, 2, 3, 4, 5
    };
    final int[] route4 = {
        5, 9, 8
    };

    driver1 = new Driver(22, route1);
    driver2 = new Driver(33, route2);
    driver3 = new Driver(44, route3);
    driver4 = new Driver(44, route4);

  }

  @Test // 1
  public void should_add_gossip_when_meet_another_driver() {
    driver1.meet(driver2);
    assertEquals(2, driver1.getGossips().size());
  }

  @Test // 3
  public void should_add_gossip_when_meet_the_same_driver_twice_if_he_knows_new_gossip() {
    driver1.meet(driver2);
    driver2.meet(driver3);
    driver1.meet(driver2);
    assertEquals(3, driver1.getGossips().size());
  }

  @Test // 12
  public void should_answer_false_when_knows_less_then_required_gossips() {
    final boolean answer = driver1.doYouKnownThisNumberOfGossips(2);
    assertEquals(false, answer);
  }

  @Test // 6
  public void should_answer_the_exact_location_when_ask_where_are_you_on_the_second_rotation() {
    final int location = driver1.getCurrentLocation(7);
    assertEquals(2, location);
  }

  @Test // 5
  public void should_answer_the_location_when_ask_where_are_you() {
    final int location = driver1.getCurrentLocation(2);
    assertEquals(1, location);
  }

  @Test // 11
  public void should_answer_true_when_knows_exaclty_the_required_gossips() {
    final boolean answer = driver1.doYouKnownThisNumberOfGossips(1);
    assertEquals(true, answer);
  }

  @Test // 10
  public void should_answer_true_when_knows_more_the_required_gossips() {
    final boolean answer = driver1.doYouKnownThisNumberOfGossips(0);
    assertEquals(true, answer);
  }

  @Test // 9
  public void should_do_nothing_when_meet_no_one() {

    driver1.meet(null);
    assertEquals(1, driver1.getGossips().size());
  }

  @Test // 1b
  public void should_have_just_One_gossip_before_meeting_others() {
    assertEquals(1, driver1.getGossips().size());
  }

  @Test // 2
  public void should_not_add_gossip_when_meet_the_same_driver_twice() {
    driver1.meet(driver2);
    driver1.meet(driver2);
    assertEquals(2, driver1.getGossips().size());
  }

  @Test // 4
  public void should_share_gossip_when_meet_another_driver() {
    driver1.meet(driver2);
    assertEquals(2, driver2.getGossips().size());
  }

  // 8
  @Test(expected = IllegalArgumentException.class)
  public void should_throws_illegal_arg_when_ask_where_are_you_on_negative_instant() {
    driver1.getCurrentLocation(-3);
  }

  // 7
  @Test(expected = IllegalArgumentException.class)
  public void should_throws_illegal_arg_when_ask_where_are_you_on_zero() {
    driver1.getCurrentLocation(0);
  }

}
