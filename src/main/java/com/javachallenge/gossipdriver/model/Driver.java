/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.javachallenge.gossipdriver.model;

import java.util.HashSet;
import java.util.Set;

public class Driver {

  private final Integer driverId;
  private final int[] route;
  private final Set<Gossip> gossips = new HashSet<Gossip>();

  public Driver(int driverId, int[] route) {
    super();
    this.driverId = driverId;
    this.route = route;
    gossips.add(new Gossip(driverId));
  }

  public boolean doYouKnownThisNumberOfGossips(int numberRequired) {
    return gossips.size() >= numberRequired;
  }

  public int getCurrentLocation(int min) {
    if (min <= 0) {
      throw new IllegalArgumentException();
    }
    return route[(min - 1) % this.route.length];

  }

  public int getDriverId() {
    return driverId;
  }

  public void meet(Driver driver) {
    if (null != driver) {
      gossips.addAll(driver.getGossips());
      driver.getGossips().addAll(gossips);
    }
  }

  public int sizeOfRoute() {

    return this.route.length;
  }

  protected Set<Gossip> getGossips() {
    return gossips;
  }

}
