/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.javachallenge.gossipdriver.model;

public class Gossip {

  private final Integer gossipId;

  Gossip(int gossipId) {
    super();
    this.gossipId = gossipId;

  }

}
