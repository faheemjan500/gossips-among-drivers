/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.javachallenge.gossipdriver;

import com.javachallenge.gossipdriver.model.Driver;

import java.util.List;

public class ManageGossipAmongDrivers {

  public int calculatingGossips2(List<Driver> driversList) {
    for (int minutes = 1; minutes <= 480; minutes++) {
      checkIfThereIsGossipToExchange(driversList, minutes);
      if (checkIfAllDriversKnowsAllGossips(driversList)) {
        return minutes;
      }
    }
    return -1;
  }

  public boolean checkIfAllDriversKnowsAllGossips(List<Driver> driversList) {
    for (final Driver driver : driversList) {
      if (!driver.doYouKnownThisNumberOfGossips(driversList.size()))
        return false;
    }
    return true;
  }

  public void checkIfThereIsGossipToExchange(List<Driver> driversList, int minutes) {
    for (int driverIndex1 = 0; driverIndex1 < driversList.size(); driverIndex1++) {
      for (int driverIndex2 = 0; driverIndex2 < driversList.size(); driverIndex2++) {
        if (driverIndex1 != driverIndex2) {
          isThereGossipAtThisStop(driversList.get(driverIndex1), driversList.get(driverIndex2),
              minutes);
        }
      }
    }
  }

  public boolean isThereGossipAtThisStop(Driver driver1, Driver driver2, int min) {
    if (driver1.getCurrentLocation(min) == driver2.getCurrentLocation(min)) {
      driver1.meet(driver2);
      return true;
    }
    return false;
  }
}